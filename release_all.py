
import os

mods = {
    "banghud": "BangHUD",
    "bccfs-reborn": "BCCFS Reborn",
    "infamyrankconverter": "InfamyRankConverter",
    "iou": "Intimidated Outlines (unsynced)",
    "jimhud": "JimHUD",
    "jimhud-compat": "JimHUD-Compat",
    "punisher": "Punisher",
    "relua": "ReLUA"
}

for k, v in mods.items():
    print("release: " + v)
    os.system('cmd /c python release.py "' + k + '" "' + v + '"')

os.system('git status')