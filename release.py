

import os
import shutil
import sys
import json
from zipfile import ZipFile

META_DIR = os.path.dirname(__file__)
MODS_DIR = META_DIR + '/../mods/'

# handle arguments
if not (len(sys.argv) == 3):
    print("usage: " + sys.argv[0] + " <identifier> <name>")
    sys.exit()
json_file = sys.argv[1] + ".json"
mod_name = sys.argv[2]

mod_dir = os.path.abspath(MODS_DIR + mod_name)
mod_file = os.path.abspath(mod_dir + "/mod.txt")

# load current json
with open(json_file, "r") as read_file:
    data = json.load(read_file)
    if "version" in data[0]:
        old_version = data[0]["version"]

with open(mod_file, "r") as read_file:
    mod = json.load(read_file)
    if "version" in mod:
        new_version = mod["version"]

# update version
print('old version: ' + old_version)
print('new version: ' + new_version)
data[0]["version"] = new_version

# dump modified json
with open(json_file, "w") as write_file:
    json.dump(data, write_file)
